<?php

namespace Drupal\rdg_ui_elements;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Utility functions for producing an options interface for Slick sliders.
 */
class CarouselOptions {

  use StringTranslationTrait;

  /**
   * Default values for each of the Slick options.
   */
  public function defaults() {
    return [
      'slick_adaptiveHeight' => FALSE,
      'slick_arrowsPlacement' => 'split',
      'slick_autoplay' => FALSE,
      'slick_autoplaySpeed' => 5000,
      'slick_centerMode' => FALSE,
      'slick_dots' => TRUE,
      'slick_draggable' => TRUE,
      'slick_fade' => TRUE,
      'slick_initialSlide' => 0,
      'slick_initialSlide_random' => FALSE,
      'slick_infinite' => TRUE,
      'slick_slidesToScroll' => 1,
      'slick_slidesToShow' => 1,
      'slick_speed' => 300,
      'slick_vertical' => FALSE,
      'slick_zIndex' => 1000,
      'slick_overrides' => FALSE,
    ];
  }

  /**
   * Create the options form.
   */
  public function form($current_values) {
    $elements = [];

    $elements['slick_adaptiveHeight'] = [
      '#type' => 'checkbox',
      '#title' => 'adaptiveHeight',
      '#description' => $this->t('Whether the slideshow will have a fixed (based on largest image) or variable height.'),
      '#default_value' => $current_values['slick_adaptiveHeight'],
    ];

    $elements['slick_arrowsPlacement'] = [
      '#type' => 'select',
      '#title' => 'arrowsPlacement',
      '#description' => $this->t('Where in the DOM to insert the previous/next arrows, in relation to the content.'),
      '#options' => [
        'beforeSlides' => $this->t('Before the slides'),
        'afterSlides' => $this->t('After the slides'),
        'split' => $this->t('Surrounding the slides'),
      ],
      '#default_value' => $current_values['slick_arrowsPlacement'],
    ];

    $elements['slick_autoplay'] = [
      '#type' => 'checkbox',
      '#title' => 'autoplay',
      '#description' => $this->t('Whether to advance the carousel automatically without user input.'),
      '#default_value' => $current_values['slick_autoplay'],
    ];

    $elements['slick_autoplaySpeed'] = [
      '#type' => 'number',
      '#title' => 'autoplay',
      '#description' => $this->t('Duration for each slide during autoplay, in ms.'),
      '#default_value' => $current_values['slick_autoplaySpeed'],
    ];

    $elements['slick_centerMode'] = [
      '#type' => 'checkbox',
      '#title' => 'centerMode',
      '#description' => $this->t('Whether to show a preview of the next/previous slide, with the current one in the center.'),
      '#default_value' => $current_values['slick_centerMode'],
    ];

    $elements['slick_dots'] = [
      '#type' => 'checkbox',
      '#title' => 'dots',
      '#description' => $this->t('Whether to show dots indicating the current slide position.'),
      '#default_value' => $current_values['slick_dots'],
    ];

    $elements['slick_draggable'] = [
      '#type' => 'checkbox',
      '#title' => 'draggable',
      '#description' => $this->t('Whether desktop users should be able to manipulate the slider by dragging.'),
      '#default_value' => $current_values['slick_draggable'],
    ];

    $elements['slick_fade'] = [
      '#type' => 'checkbox',
      '#title' => 'fade',
      '#description' => $this->t('Whether to use a fade transition instead of a slide transition (recommended to reduce motion sensitivity issues).'),
      '#default_value' => $current_values['slick_fade'],
    ];

    $elements['slick_initialSlide'] = [
      '#type' => 'number',
      '#title' => 'initialSlide',
      '#description' => $this->t('The slide number (0-indexed) to display on page load.'),
      '#default_value' => $current_values['slick_initialSlide'],
    ];

    $elements['slick_initialSlide_random'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Randomize initial slide'),
      '#description' => $this->t('If checked, the initial slide will be random, overriding the above setting.'),
      '#default_value' => $current_values['slick_initialSlide_random'],
    ];

    $elements['slick_infinite'] = [
      '#type' => 'checkbox',
      '#title' => 'infinite',
      '#description' => $this->t('Whether to treat the slides as an infinite cycle.'),
      '#default_value' => $current_values['slick_infinite'],
    ];

    $elements['slick_slidesToScroll'] = [
      '#type' => 'number',
      '#title' => 'slidesToScroll',
      '#description' => $this->t('The number of slides to advance on each click of the "next" button.'),
      '#default_value' => $current_values['slick_slidesToScroll'],
    ];

    $elements['slick_slidesToShow'] = [
      '#type' => 'number',
      '#title' => 'slidesToShow',
      '#description' => $this->t('The number of slides to show at a time.'),
      '#default_value' => $current_values['slick_slidesToShow'],
    ];

    $elements['slick_speed'] = [
      '#type' => 'number',
      '#title' => 'speed',
      '#description' => $this->t('The transition speed between slides, in ms.'),
      '#default_value' => $current_values['slick_speed'],
    ];

    $elements['slick_vertical'] = [
      '#type' => 'checkbox',
      '#title' => 'vertical',
      '#description' => $this->t('Whether to display the slides vertically, rather than horizontally.'),
      '#default_value' => $current_values['slick_vertical'],
    ];

    $elements['slick_zIndex'] = [
      '#type' => 'number',
      '#title' => 'zIndex',
      '#description' => $this->t('The Z-Index of each slide, sometimes useful for browser compatibility tweaks.'),
      '#default_value' => $current_values['slick_zIndex'],
    ];

    $elements['slick_overrides'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additional settings (JSON format)'),
      '#description' => $this->t('Example: <br><em>{ "arrows": "false", "responsive": [{ "breakpoint": 1000, "settings": { "slidesToShow": 2, "slidesToScroll": 2 }},{ "breakpoint": 500, "settings": { "slidesToShow": 1, "slidesToScroll": 1 }}]}</em>'),
      '#default_value' => $current_values['slick_overrides'],
    ];

    return $elements;
  }

  /**
   * Return a Javascript-ready array of variables for Slick.
   */
  public function slickVariables(array $options) {
    $variables = [
      'adaptiveHeight' => $options['slick_adaptiveHeight'],
      'arrowsPlacement' => $options['slick_arrowsPlacement'],
      'autoplay' => $options['slick_autoplay'],
      'autoplaySpeed' => $options['slick_autoplaySpeed'],
      'centerMode' => $options['slick_centerMode'],
      'dots' => $options['slick_dots'],
      'draggable' => $options['slick_draggable'],
      'fade' => $options['slick_fade'],
      'initialSlide' => $options['slick_initialSlide'],
      'initialSlide_random' => $options['slick_initialSlide_random'],
      'infinite' => $options['slick_infinite'],
      'slidesToScroll' => $options['slick_slidesToScroll'],
      'slidesToShow' => $options['slick_slidesToShow'],
      'speed' => $options['slick_speed'],
      'vertical' => $options['slick_vertical'],
      'zIndex' => $options['slick_zIndex'],
    ];

    $overrides = Json::decode($options['slick_overrides']);
    if (is_array($overrides)) {
      $variables = array_merge_recursive($variables, $overrides);
    }

    return $variables;
  }

}
