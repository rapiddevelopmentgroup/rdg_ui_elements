<?php

namespace Drupal\rdg_ui_elements\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Creates a tabs component.
 *
 * @RenderElement("rdg_tabs")
 */
class Tabs extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'rdg_ui_elements_tabs',
      '#attributes' => [],
      '#attached' => [
        'library' => ['rdg_ui_elements/tabs'],
      ],
      '#sections' => [],
      '#active_index' => 0,
      '#active_id' => '',
      '#pre_render' => [
        [self::class, 'preRenderTabs'],
      ],
    ];
  }

  /**
   * Pre-render callback.
   *
   * @param array $element
   *   The renderable array representing the rdg_tabs element.
   *
   * @return array
   *   The passed in element with changes made to attributes depending on
   *   context.
   */
  public static function preRenderTabs(array $element) {
    $default_id = Html::getUniqueId('tabs');

    foreach ($element['#sections'] as $index => &$section) {
      if (empty($section['id'])) {
        $section['id'] = $default_id . '-' . $index;
      }
      if ($index == $element['#active_index']) {
        $element['#active_id'] = $section['id'];
      }
    }

    return $element;
  }

}
