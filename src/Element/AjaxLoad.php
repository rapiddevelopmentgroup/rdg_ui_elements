<?php

namespace Drupal\rdg_ui_elements\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Creates an component that loads content lazy.
 *
 * @RenderElement("rdg_ajax_load")
 */
class AjaxLoad extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'rdg_ui_elements_ajax_load',
      '#attributes' => [],
      '#callback_url' => NULL,
      '#loading_message' => 'Loading',
      '#attached' => [
        'library' => ['rdg_ui_elements/ajax-load'],
      ],
      '#pre_render' => [
        [self::class, 'preRenderAjaxLoad'],
      ],
    ];
  }

  /**
   * Pre-render callback.
   *
   * @param array $element
   *   The renderable array representing the rdg_ui_elements_ajax_load element.
   *
   * @return array
   *   The passed in element with changes made to attributes depending on
   *   context.
   */
  public static function preRenderAjaxLoad(array $element) {
    $render = [];

    $loading_message = '<div class="loading-message">' . $element['#loading_message'] . '</div>';

    $render['container'] = [
      '#prefix' => '<div class="lazy-load loading" data-callback-url="' . $element['#callback_url']->toString() . '">' . $loading_message,
      '#suffix' => '</div>',
      '#theme' => 'rdg_ui_elements_ajax_load',
      '#attached' => [
        'library' => ['rdg_ui_elements/ajax-load'],
      ],
    ];

    return $render;
  }

}
