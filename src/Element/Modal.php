<?php

namespace Drupal\rdg_ui_elements\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Creates a modal component.
 *
 * @RenderElement("rdg_modal")
 */
class Modal extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'rdg_ui_elements_modal',
      '#attached' => [
        'library' => ['rdg_ui_elements/modal'],
      ],
      '#pre_render' => [
        [self::class, 'preRenderModal'],
      ],
    ];
  }

  /**
   * Pre-render callback.
   *
   * @param array $element
   *   The renderable array representing the rdg_modal element.
   *
   * @return array
   *   The passed in element with changes made to attributes depending on
   *   context.
   */
  public static function preRenderModal(array $element) {
    if (empty($element['#id'])) {
      $id = 'modal';
    }
    else {
      $id = $element['#id'];
    }
    $element['#id'] = Html::getUniqueId($id);

    return $element;
  }

}
