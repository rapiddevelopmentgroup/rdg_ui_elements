<?php

namespace Drupal\rdg_ui_elements\Element;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Creates an accordion component.
 *
 * @RenderElement("rdg_accordion")
 */
class Accordion extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'rdg_ui_elements_accordion',
      '#attributes' => [],
      '#attached' => [
        'library' => ['rdg_ui_elements/accordion'],
      ],
      '#sections' => [],
      '#exclusive' => TRUE,
      '#pre_render' => [
        [self::class, 'preRenderAccordion'],
      ],
    ];
  }

  /**
   * Pre-render callback.
   *
   * @param array $element
   *   The renderable array representing the rdg_accordion element.
   *
   * @return array
   *   The passed in element with changes made to attributes depending on
   *   context.
   */
  public static function preRenderAccordion(array $element) {
    $default_id = Html::getUniqueId('accordion');

    foreach ($element['#sections'] as $index => &$section) {
      if (empty($section['id'])) {
        $section['id'] = $default_id . '-' . $index;
      }
      if (empty($section['expanded'])) {
        $section['expanded'] = FALSE;
      }
    }

    $element['#attributes']['data-exclusive'] = (!empty($element['#exclusive']) ? 'true' : 'false');

    return $element;
  }

}
