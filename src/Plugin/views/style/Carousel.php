<?php

namespace Drupal\rdg_ui_elements\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Displays items in an accessible carousel.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "rdg_carousel",
 *   title = @Translation("Carousel"),
 *   help = @Translation("Displays items in an accessible carousel."),
 *   theme = "rdg_ui_elements_view_carousel",
 *   display_types = {"normal"}
 * )
 */
class Carousel extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $optionsService = \Drupal::service('rdg_ui_elements.carousel.options');

    $options += array_map(function($x) {
      return ['default' => $x];
    }, $optionsService->defaults());
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $optionsService = \Drupal::service('rdg_ui_elements.carousel.options');

    $form += $optionsService->form($this->options);
  }

}
