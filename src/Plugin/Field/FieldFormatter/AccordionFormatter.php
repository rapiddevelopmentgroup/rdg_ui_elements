<?php

namespace Drupal\rdg_ui_elements\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'rr_ui_elements_accordion' formatter.
 *
 * @FieldFormatter(
 *   id = "rr_ui_elements_accordion",
 *   label = @Translation("Accordion"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class AccordionFormatter extends TextDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    foreach ($elements as $index => $element) {
      $new_element = [
        '#type' => 'rdg_accordion',
        '#exclusive' => FALSE,
        '#sections' => [
          [
            'title' => [
              '#type' => 'html_tag',
              '#tag' => 'h3',
              '#value' => $items->getFieldDefinition()->getLabel(),
            ],
            'content' => $element,
            '#attributes' => [
              'class' => ['field'],
            ],
          ],
        ],
      ];
      $elements[$index] = $new_element;
    }

    return $elements;
  }

}
