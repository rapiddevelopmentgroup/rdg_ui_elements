<?php

namespace Drupal\rdg_ui_elements\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'rdg_accordion_paragraphs' formatter.
 *
 * @FieldFormatter(
 *   id = "rdg_accordion_paragraphs",
 *   label = @Translation("Accordion"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class AccordionParagraphsFormatter extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'title_field' => 'field_accordion_title',
      'exclusive' => FALSE,
      'expand_first' => FALSE,
      'root_heading_level' => 2,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['title_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title field'),
      '#default_value' => $this->getSetting('title_field'),
      '#required' => TRUE,
    ];

    $elements['exclusive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show one item at a time'),
      '#default_value' => $this->getSetting('exclusive'),
    ];

    $elements['expand_first'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Expand the first item on page load'),
      '#default_value' => $this->getSetting('expand_first'),
    ];

    $elements['root_heading_level'] = [
      '#type' => 'number',
      '#title' => $this->t('Root heading level'),
      '#default_value' => $this->getSetting('root_heading_level'),
      '#description' => $this->t('HTML heading level for accordion items. This will be automatically increased by 1 for each level of nested accordion items, if any.'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $children = parent::viewElements($items, $langcode);

    $accordion = [
      '#type' => 'rdg_accordion',
      '#sections' => [],
      '#exclusive' => $this->getSetting('exclusive'),
    ];

    foreach ($items as $delta => $item) {
      $accordion['#sections'][] = [
        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'h' . $this->headingLevel($items->getEntity()),
          '#value' => $item->entity->{$this->getSetting('title_field')}->value,
        ],
        'content' => $children[$delta],
        'expanded' => $this->getSetting('expand_first') && $delta == 0,
      ];
    }

    return $accordion;
  }

  /**
   * Calculate the heading level for a paragraph section entity.
   */
  protected function headingLevel(ParagraphInterface $paragraph) {
    $count = $this->getSetting('root_heading_level');
    $current_entity = $paragraph;
    while ($current_entity) {
      if ($current_entity->_referringItem) {
        $current_entity = $current_entity->_referringItem->getEntity() ?? NULL;
      }
      else {
        $current_entity = NULL;
      }

      if ($current_entity && $current_entity->getEntityTypeId() == 'paragraph' && $current_entity->getType() == 'accordion_section') {
        $count++;
      }
    }
    return $count;
  }

}
