<?php

namespace Drupal\rdg_ui_elements\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rdg_ui_elements\CarouselOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'book_edition_links' formatter.
 *
 * @FieldFormatter(
 *   id = "rdg_carousel_paragraphs",
 *   label = @Translation("Carousel slides"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class Carousel extends EntityReferenceEntityFormatter {

  /**
   * The options service.
   *
   * @var \Drupal\rdg_ui_elements\CarouselOptions
   */
  protected CarouselOptions $optionsService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->optionsService = $container->get('rdg_ui_elements.carousel.options');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $optionsService = \Drupal::service('rdg_ui_elements.carousel.options');
    return $optionsService->defaults() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements += $this->optionsService->form($this->getSettings());

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('slick_overrides')) {
      $json = Json::decode($this->getSetting('slick_overrides'));
      if ($json === NULL) {
        $summary[] = $this->t('Warning: JSON improperly formatted.');
      }
      elseif ($json) {
        $summary[] = $this->t('Additional settings: @overrides', [
          '@overrides' => $this->getSetting('slick_overrides'),
        ]);
      }
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $elements['#attached']['library'][] = 'rdg_ui_elements/carousel';

    // Get a unique ID for splitting settings between multiple carousels on the same page.
    $unique_id = Html::getUniqueId('rdg-carousel');
    // Set the unique id as a class on the element for targeting.
    $elements['#attributes']['id'] = $unique_id;
    // Add settings to the element.
    $elements['#attached']['drupalSettings']['slick']['carousels'][$unique_id] = $this->optionsService->slickVariables($this->getSettings());
    // Send the CSS target to settings to target the correct carousel.
    $elements['#attached']['drupalSettings']['slick']['carousels'][$unique_id]['target'] = $unique_id;

    $elements['#theme'] = 'rdg_ui_elements_paragraphs_carousel';
    return $elements;
  }

}
