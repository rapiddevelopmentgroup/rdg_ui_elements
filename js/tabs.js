/**
 * @file tabs.js
 *
 * Tabs component.
 */
(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.rdg_tabs = {
    attach: function (context, settings) {
      $(once("rdg_tabs", ".tabs", context)).each(function () {
        var $tabGroup = $(this);
        var $tabList = $tabGroup.find("[role=tablist]");
        var $tabs = $tabGroup.find("[role=tab]");
        var $tabPanels = $tabGroup.find("[role=tabpanel]");
        var activeTabNumber = $tabs.find("[aria-selected=true]").index();

        /**
         * Activate the tab content corresponding to a given tab.
         * @param {jQuery} $tab The tab that will become selected.
         */
        function selectTab($tab) {
          var thisTabId = $tab.attr("id");
          activeTabNumber = $tabs.index($tab);
          var $thisTabPanel = $tabPanels.filter(
            '[aria-labelledby="' + thisTabId + '"]'
          );

          // Mark this tab as selected.
          $tabs.attr({
            "aria-selected": false,
            tabindex: -1,
          });
          $tab.attr({
            "aria-selected": true,
            tabindex: 0,
          });

          // Show the selected panel.
          $tabPanels.attr({
            "aria-hidden": true,
            tabindex: -1,
          });
          $thisTabPanel.attr({
            "aria-hidden": false,
            tabindex: 0,
          });

          $tab.focus();
        }

        // Add a click event handler to each tab
        $tabs.click(function () {
          selectTab($(this));
        });

        // Enable arrow navigation between tabs in the tab list

        // horizontal tabs (tabs-top) keydown for left/right arrows
        $tabList.on("keydown", function (e) {
          if (
            e.keyCode === 39 ||
            e.keyCode === 37 ||
            e.keyCode === 40 ||
            e.keyCode === 38
          ) {
            // Prevent window scrolling on up/down keys.
            if (e.keyCode === 40 || e.keyCode === 38) {
              e.preventDefault();
            }
            // Move right/down.
            if (e.keyCode === 39 || e.keyCode === 40) {
              activeTabNumber++;
              // If we're at the end, go to the start
              if (activeTabNumber >= $tabs.length) {
                activeTabNumber = 0;
              }
            }
            // Move left/up.
            else if (e.keyCode === 37 || e.keyCode === 38) {
              activeTabNumber--;
              // If we're at the start, move to the end
              if (activeTabNumber < 0) {
                activeTabNumber = $tabs.length - 1;
              }
            }

            selectTab($tabs.eq(activeTabNumber));
          }
        });
      });
    },
  };
})(jQuery, Drupal);
