/**
 * @file
 * JavaScript for loading content via AJAX.
 */

(function ($) {
  "use strict";

  Drupal.behaviors.rdgAjaxLoad = {
    attach: function (context, settings) {
      $(once("rdgAjaxLoad", ".lazy-load", context)).each(function () {
        var $container = $(this);
        var callbackUrl = $container.data("callback-url");
        console.log("callbackUrl", callbackUrl);
        $.getJSON(callbackUrl, function (data) {
          $container.removeClass("loading");
          $container.html(data.html);
          Drupal.attachBehaviors($container[0], settings);
        });
      });
    },
  };
})(jQuery);
