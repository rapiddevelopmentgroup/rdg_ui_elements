(function ($, Drupal) {
  Drupal.behaviors.rdgCarousel = {
    attach: function (context, settings) {
      $(".slick-carousel", context).each(function () {
        const id = $(this).attr("id");
        const carousel = settings.slick.carousels[id];
        const base_options = {
          adaptiveHeight: !!carousel.adaptiveHeight,
          arrowsPlacement: carousel.arrowsPlacement,
          autoplay: !!carousel.autoplay,
          autoplaySpeed: +carousel.autoplaySpeed,
          centerMode: !!carousel.centerMode,
          dots: !!carousel.dots,
          draggable: !!carousel.draggable,
          fade: !!carousel.fade,
          initialSlide: carousel.initialSlide_random
            ? Math.floor(Math.random() * $carousel.children().length)
            : +carousel.initialSlide,
          infinite: !!carousel.infinite,
          slidesToScroll: +carousel.slidesToScroll,
          slidesToShow: +carousel.slidesToShow,
          speed: +carousel.speed,
          vertical: !!carousel.vertical,
          verticalSwiping: !!carousel.vertical,
          zIndex: +carousel.zIndex,
        };

        // Merge in settings added via "Additional settings (json)" formatter field
        const options = $.extend(carousel, base_options);

        $(this).slick(options);
      });
    },
  };
})(jQuery, Drupal);
