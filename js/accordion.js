/**
 * @file accordion.js
 *
 * Accordion reveal/hide component.
 */
(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.rdg_accordion = {
    attach: function (context, settings) {
      $(once("rdg_accordion", ".accordion", context)).each(function () {
        const $accordion = $(this);
        $accordion.children(".accordion__button").on("click", function () {
          const $button = $(this);
          const $content = $("#" + $button.attr("aria-controls"));
          if ($button.attr("aria-expanded") == "true") {
            $button.attr("aria-expanded", "false");
            $content.attr("aria-hidden", "true");
          } else {
            $button.attr("aria-expanded", "true");
            $content.attr("aria-hidden", "false");

            if ($accordion.data("exclusive")) {
              $accordion
                .find(".accordion__button")
                .not($button)
                .attr("aria-expanded", "false");
              $accordion
                .find(".accordion__content")
                .not($content)
                .attr("aria-hidden", "true");
            }
          }
        });
      });
    },
  };
})(jQuery, Drupal);
