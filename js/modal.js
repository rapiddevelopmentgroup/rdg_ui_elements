(function ($, Drupal) {
  Drupal.behaviors.rdgModal = {
    attach: function (context, settings) {
      MicroModal.init({
        onShow: function (modal, trigger) {
          $(document).trigger("rdgModalOpen", modal);
        },
        onClose: function (modal, trigger) {
          // Reload frame contents, to kill ongoing processes like videos.
          var iframe = modal.querySelector("iframe");
          if (iframe !== null) {
            var iframeSrc = iframe.src;
            iframe.src = iframeSrc;
          }
          $(document).trigger("rdgModalClose", modal);
        },
      });
    },
  };
})(jQuery, Drupal);
