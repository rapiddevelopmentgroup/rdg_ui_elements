<?php

namespace Drupal\rdg_ui_elements_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Examples of UI elements.
 */
class Accordion extends ControllerBase {

  /**
   * Demonstrate rdg_accordion.
   */
  public function accordionExample() {
    return [
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h1',
        '#value' => 'Accordion',
      ],
      'accordion' => [
        '#type' => 'rdg_accordion',
        '#exclusive' => FALSE,
        '#sections' => [
          [
            'id' => 'hello-world',
            'title' => 'Hello',
            'content' => [
              '#type' => 'html_tag',
              '#tag' => 'p',
              '#value' => 'World',
            ],
          ],
          [
            'title' => 'Goodnight',
            'content' => [
              '#type' => 'html_tag',
              '#tag' => 'p',
              '#value' => 'Moon',
            ],
          ],
        ],
        '#attributes' => [
          'data-foo' => 'bar',
          'class' => ['test-class'],
        ],
      ],
    ];
  }

}
