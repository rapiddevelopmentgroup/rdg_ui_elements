<?php

namespace Drupal\rdg_ui_elements_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Examples of UI elements.
 */
class Modal extends ControllerBase {

  /**
   * Demonstrate modal.
   */
  public function modalExample() {
    return [
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h1',
        '#value' => 'Modal',
      ],
      'simple-modal' => [
        '#type' => 'rdg_modal',
        '#content' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => 'How razorback-jumping frogs can level six piqued gymnasts!',
        ],
      ],
      'complex-modal' => [
        '#type' => 'rdg_modal',
        '#id' => 'complex-modal',
        '#trigger' => [
          '#type' => 'plain_text',
          '#plain_text' => 'Open modal with all the options specified',
        ],
        '#title' => $this->t('The title of the modal'),
        '#content' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => 'The quick brown fox jumps over the lazy dog.',
        ],
        '#footer' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => 'Some content for the footer area.',
        ],
      ],
    ];
  }

}
