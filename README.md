# RDG UI Elements module #

How to add the RDG UI Elements module to a Drupal 8 project.

### Setup ###

You'll have to make modifications to the site's `composer.json` file in order to install the module via composer.

1. Under `"repositories"`, add entries for asset-packagist and rapiddg packages, after the `packages.drupal.org` entry:
```
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "composer",
            "url": "https://asset-packagist.org"
        },
        {
            "type": "composer",
            "url": "https://packages.rapiddg.com"
        }
    ],
    ...
```

2. Under `"extra"`, add an `"installer-types"` entry for bower and npm assets, after `"drupal-scaffold"`:
```
    "extra": {
        "drupal-scaffold": {
            "locations": {
                "web-root": "web/"
            }
        },
        "installer-types": [
            "bower-asset",
            "npm-asset"
        ],
        ...
```

3. Under `"installer-paths"`, add `bower-asset` and `npm-asset` types to the `"web/libraries/${name}"` entry
```
    "installer-paths": {
        "web/core": [
            "type:drupal-core"
           ],
        "web/libraries/{$name}": [
            "type:drupal-library",
            "type:bower-asset",
            "type:npm-asset"
        ],
        ...
```

4. Add the RDG UI Elements library: `composer require rapiddevelopmentgroup/rdg_ui_elements`

5. Check to see if the module assets installed properly in the project root under `web/libraries`
